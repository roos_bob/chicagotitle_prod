/**
 * Deeded_Owner_List__c trigger
 *   
 * @author CRMCulture  
 * @version 1.00 
 * @param Platform after insert, after update, before insert, before update
*/ 
trigger Deeded_Owner_ListTrigger on Deeded_Owner_List__c (	before insert, 
	before update, 
	before delete, 
	after insert, 
	after update, 
	after delete, 
	after undelete) {
	TriggerFactory.createTriggerDispatcher(Deeded_Owner_List__c.sObjectType);
}