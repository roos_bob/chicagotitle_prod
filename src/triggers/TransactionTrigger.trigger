/**
 * Created by bob.roos on 2/27/2017.
 */

trigger TransactionTrigger on Transaction__c (
        before insert,
        before update,
        before delete,
        after insert,
        after update,
        after delete,
        after undelete) {
    TriggerFactory.createTriggerDispatcher(Transaction__c.sObjectType);
}