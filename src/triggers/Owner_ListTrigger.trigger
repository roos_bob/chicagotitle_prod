/**
 * Created by bob.roos on 2/16/2017.
 */

trigger Owner_ListTrigger on Owner_List__c (
        before insert,
        before update,
        before delete,
        after insert,
        after update,
        after delete,
        after undelete) {
    TriggerFactory.createTriggerDispatcher(Owner_List__c.sObjectType);
}