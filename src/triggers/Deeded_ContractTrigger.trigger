/**
 * Deeded_Contract__c trigger
 *   
 * @author CRMCulture  
 * @version 1.00 
 * @param Platform after insert, after update, before insert, before update
*/ 
trigger Deeded_ContractTrigger on Deeded_Contract__c (
	before insert,
	before update,
	before delete,
	after insert,
	after update,
	after delete,
	after undelete) {
	TriggerFactory.createTriggerDispatcher(Deeded_Contract__c.sObjectType);
}