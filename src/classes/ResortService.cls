global class ResortService
{
	webservice static void updateInventory(Id resortId, String userEmail)
	{
		CS_Batch_Process_Setting__c batchProcessSetting = CS_Batch_Process_Setting__c.getInstance('Unit_Inventory_BatchInsert');
        Integer batchSize = (Integer)batchProcessSetting.Batch_Size__c;
		Database.executeBatch(new Unit_Inventory_BatchInsert(resortId, userEmail), batchSize);
	}
}