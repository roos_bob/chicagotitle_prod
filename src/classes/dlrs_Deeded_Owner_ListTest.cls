/**
 * Auto Generated and Deployed by the Declarative Lookup Rollup Summaries Tool package (dlrs)
 **/
@IsTest
private class dlrs_Deeded_Owner_ListTest
{
    @IsTest
    private static void testTrigger()
    {
        // Force the dlrs_Deeded_Owner_ListTrigger to be invoked, fails the test if org config or other Apex code prevents this.
        dlrs.RollupService.testHandler(new Deeded_Owner_List__c());
    }
}