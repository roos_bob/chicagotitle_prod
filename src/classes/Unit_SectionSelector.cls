public with sharing class Unit_SectionSelector extends fflib_SObjectSelector
{
	public List<Schema.SObjectField> getSObjectFieldList()
	{
		return new List<Schema.SObjectField> {
			Unit_Section__c.Id,
			Unit_Section__c.Name,
			Unit_Section__c.UnitId__c,
			Unit_Section__c.Search_Code__c,
			Unit_Section__c.ICN_Code__c
		};
	}
	
	public Schema.SObjectType getSObjectType()
	{
		return Unit_Section__c.sObjectType;
	}

	public List<Unit_Section__c> selectById(Set<ID> idSet)
	{
		return (List<Unit_Section__c>) selectSObjectsById(idSet);
	}
	
	public fflib_QueryFactory initNewQueryFactory() {
		fflib_QueryFactory query = newQueryFactory();
		query.selectField('UnitId__r.ICN_Code__c');
		return query;
	}

	public List<Unit_Section__c> selectByName(List<String> nameList)
	{
		fflib_QueryFactory query = initNewQueryFactory();
		query.setCondition('Name IN :nameList');
		return  (List<Unit_Section__c>) Database.query(query.toSOQL());
	}

	//public List<Unit_Section__c> selectByName(Set<String> nameSet)
	//{
		//fflib_QueryFactory query = initNewQueryFactory();
		//query.setCondition('Name IN :nameSet');
		//return  (List<Unit_Section__c>) Database.query(query.toSOQL());
	//}

	public Map<String, Unit_Section__c> selectMapBySearchCode(Set<String> codes)
	{
		fflib_QueryFactory query = initNewQueryFactory();
		query.setCondition('Search_Code__c IN :codes');
		List<Unit_Section__c> records = (List<Unit_Section__c>) Database.query(query.toSOQL());

		Map<String, Unit_Section__c> resultMap = new Map<String, Unit_Section__c>();
		for (Unit_Section__c record: records) {
			resultMap.put(record.Search_Code__c, record);
		}
		return resultMap;

	}

}