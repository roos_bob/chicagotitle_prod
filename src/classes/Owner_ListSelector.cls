public with sharing class Owner_ListSelector extends fflib_SObjectSelector
{
	public List<Schema.SObjectField> getSObjectFieldList()
	{
		return new List<Schema.SObjectField> {
			Owner_List__c.Id,
			Owner_List__c.Name,
			Owner_List__c.Transaction__c, 
			Owner_List__c.Contract__c, 
			Owner_List__c.Current_Owner_of_Contract__c
		};
	}
	
	public Schema.SObjectType getSObjectType()
	{
		return Owner_List__c.sObjectType;
	}

	public fflib_QueryFactory initNewQueryFactory() {
		fflib_QueryFactory query = newQueryFactory();
		return query;
	}

	public List<Owner_List__c> selectById(Set<ID> idSet)
	{
		return (List<Owner_List__c>) selectSObjectsById(idSet);
	}

	public Map<Id,Owner_List__c> selectMapById(Set<Id> idSet) {
		return new Map<Id, Owner_List__c>(selectById(idSet));
	}

	public List<Owner_List__c> selectByName(Set<String> names)
	{
		fflib_QueryFactory query = initNewQueryFactory();
		query.setCondition('Name IN :names');
		//query.selectField('Last_Owner_Trans__c');
		return  (List<Owner_List__c>) Database.query(query.toSOQL());
	}


	public Map<String, Owner_List__c> selectMapByName(Set<String> names)
	{
		fflib_QueryFactory query = initNewQueryFactory();
		query.setCondition('Name IN :names');
		List<Owner_List__c> records = (List<Owner_List__c>) Database.query(query.toSOQL());

		Map<String, Owner_List__c> resultMap = new Map<String, Owner_List__c>();
		for (Owner_List__c record: records) {
			resultMap.put(record.Name, record);
		}
		return resultMap;
	}

	public List<Owner_List__c> selectByContract(Set<Id> idSet)
	{
		fflib_QueryFactory query = initNewQueryFactory();
		query.setCondition('Contract__c IN :idSet');
		query.selectField('Owner__r.Name');
		System.debug(query.toSOQL());
		return  (List<Owner_List__c>) Database.query(query.toSOQL());
	}


}