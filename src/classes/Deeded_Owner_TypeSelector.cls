public with sharing class Deeded_Owner_TypeSelector extends fflib_SObjectSelector
{
	public List<Schema.SObjectField> getSObjectFieldList()
	{
		return new List<Schema.SObjectField> {
			Deeded_Owner_Type__c.Id,
			Deeded_Owner_Type__c.Name
		};
	}
	
	public Schema.SObjectType getSObjectType()
	{
		return Deeded_Owner_Type__c.sObjectType;
	}

	public List<Deeded_Owner_Type__c> selectById(Set<ID> idSet)
	{
		return (List<Deeded_Owner_Type__c>) selectSObjectsById(idSet);
	}

	public fflib_QueryFactory initNewQueryFactory() {
		fflib_QueryFactory query = newQueryFactory();
		return query;
	}

	public List<Deeded_Owner_Type__c> selectByName(List<String> nameList)
	{
		fflib_QueryFactory query = initNewQueryFactory();
		query.setCondition('Name IN :nameList');
		return  (List<Deeded_Owner_Type__c>) Database.query(query.toSOQL());
	}

	public List<Deeded_Owner_Type__c> selectByName(String name)
	{
		fflib_QueryFactory query = initNewQueryFactory();
		query.setCondition('Name = :name');
		return  (List<Deeded_Owner_Type__c>) Database.query(query.toSOQL());
	}

	public Id selectDefault()
	{
		fflib_QueryFactory query = initNewQueryFactory();
		query.setCondition('Is_Batch_Import_Default__c = true');
		List<Deeded_Owner_Type__c> ots = (List<Deeded_Owner_Type__c>) Database.query(query.toSOQL());
		Id retId = null;
		if (ots.size() > 0) {
			retId = ots[0].Id;
		}
		return retId;
	}


}