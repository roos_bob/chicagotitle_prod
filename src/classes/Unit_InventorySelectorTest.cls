@isTest
private class Unit_InventorySelectorTest {

	@isTest
	private static void testSelector() {

		Unit_Inventory__c record = new Unit_Inventory__c(Name = 'test');
		insert record;

		Unit_InventorySelector selector = new Unit_InventorySelector();
		Set<Id> idSet = new Set<Id> {record.Id};
		List<String> nameList = new List<String> {'test'};
		Set<String> nameSet = new Set<String> {'test'};
		Set<String> ICNSet = new Set<String>();
		
		List<Unit_Inventory__c> byID = selector.selectById(idSet);
		System.assert(byId.size() == 1);

		List<Unit_Inventory__c> byName = selector.selectByName(nameList);
		System.assert(byName.size() == 1);
		
		Map<String, Unit_Inventory__c> mapByName = selector.selectMapByICN(ICNSet);
		System.assert(mapByName.size() == 0);

	}
}