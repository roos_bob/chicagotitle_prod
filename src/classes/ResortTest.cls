@isTest
private class ResortTest {
	@testSetup static void setupData() {
	
	// Create Batch Process Setting
		CS_Batch_Process_Setting__c setting = new CS_Batch_Process_Setting__c();
		setting.Name = 'Unit_Inventory_BatchInsert';
		setting.Batch_Size__c = 10;
		insert setting;

	// Create Developer
		Map<String, Id> developerRecordTypes =  UtilityClass.getRecordTypeIdsByDeveloperName(Developer__c.SObjectType); 
		Id developerRTId;

		if(developerRecordTypes.containsKey('Deeded')){
			developerRTId = developerRecordTypes.get('Deeded');
		} else {
			System.assert(false, 'Missing Deeded record type for Developer__c');
		}

		Developer__c d = new Developer__c(Name = 'Grand Pacific', Active__c = true, RecordTypeId = developerRTId);
		insert d;

	// Create Resort
		Map<String, Id> resortRecordTypes =  UtilityClass.getRecordTypeIdsByDeveloperName(Resort__c.SObjectType); 
		Id resortRTId;

		if(resortRecordTypes.containsKey('Deeded')){
			resortRTId = resortRecordTypes.get('Deeded');
		} else {
			System.assert(false, 'Missing Deeded record type for Resort__c');
		}

		Resort__c r = new Resort__c(Name = 'Grand Pacific MarBrisa Resort', RecordTypeId = resortRTId, Developer__c = d.Id, Code__c = 'GMP');
		insert r;

	// Create Weeks
		Map<String, Week__c> wks = new Map<String, Week__c>();
		wks.put('01', new Week__c(Name = '01', ICN_Code__c = '01', Resort__c = r.Id, Season__c = 'Gold'));
		insert wks.values();

	// Create Usage Types
		Map<String, Usage_Type__c> uts = new Map<String, Usage_Type__c>();
		uts.put('Annual', new Usage_Type__c(Name = 'Annual', ICN_Code__c = 'Z', Resort__c = r.Id));
		uts.put('Biennial (Odd)', new Usage_Type__c(Name = 'Biennial (Odd)', ICN_Code__c = 'O', Resort__c = r.Id));
		uts.put('Biennial (Even)', new Usage_Type__c(Name = 'Biennial (Even)', ICN_Code__c = 'E', Resort__c = r.Id));
		insert uts.values();

	// Create Overlapping Usage Types
		List<Overlapping_Usage_Type__c> outs = new List<Overlapping_Usage_Type__c>();
		outs.add(new Overlapping_Usage_Type__c(Usage_TypeId__c = uts.get('Annual').Id, Overlaps_WithId__c = uts.get('Biennial (Odd)').Id));
		outs.add(new Overlapping_Usage_Type__c(Usage_TypeId__c = uts.get('Annual').Id, Overlaps_WithId__c = uts.get('Biennial (Even)').Id));
		outs.add(new Overlapping_Usage_Type__c(Usage_TypeId__c = uts.get('Biennial (Odd)').Id, Overlaps_WithId__c = uts.get('Annual').Id));
		outs.add(new Overlapping_Usage_Type__c(Usage_TypeId__c = uts.get('Biennial (Even)').Id, Overlaps_WithId__c = uts.get('Annual').Id));
		insert outs;

	// Create Section Types
		Map<String, Section_Type__c> sts = new Map<String, Section_Type__c>();
		sts.put('Full', new Section_Type__c(Name = 'Full', ResortId__c = r.Id));
		sts.put('Bedroom', new Section_Type__c(Name = 'Bedroom', ResortId__c = r.Id));
		sts.put('Studio', new Section_Type__c(Name = 'Studio', ResortId__c = r.Id));
		insert sts.values();

	// Create Overlapping Section Types
		List<Overlapping_Section_Type__c> osts = new List<Overlapping_Section_Type__c>();
		osts.add(new Overlapping_Section_Type__c(Section_TypeId__c = sts.get('Full').Id, Overlaps_WithId__c = sts.get('Bedroom').Id));
		osts.add(new Overlapping_Section_Type__c(Section_TypeId__c = sts.get('Full').Id, Overlaps_WithId__c = sts.get('Studio').Id));
		osts.add(new Overlapping_Section_Type__c(Section_TypeId__c = sts.get('Bedroom').Id, Overlaps_WithId__c = sts.get('Full').Id));
		osts.add(new Overlapping_Section_Type__c(Section_TypeId__c = sts.get('Studio').Id, Overlaps_WithId__c = sts.get('Full').Id));
		insert osts;

	// Create Unit
		Unit__c u = new Unit__c(Name = '5811', ResortId__c = r.Id, ICN_Code__c = '5811', Type__c = '2.7', View__c = 'Partk');
		insert u;

	// Create Unit Sections
		Map<String, Unit_Section__c> uss = new Map<String, Unit_Section__c>();
		uss.put('5811', new Unit_Section__c(Name = '5811', ICN_Code__c = 'A1', UnitId__c = u.Id, Size__c = '2BR/2BA', Section_TypeId__c = sts.get('Full').Id));
		uss.put('58111', new Unit_Section__c(Name = '58111', ICN_Code__c = 'B1', UnitId__c = u.Id, Size__c = '1BR/1BA', Section_TypeId__c = sts.get('Bedroom').Id));
		uss.put('58112', new Unit_Section__c(Name = '58112', ICN_Code__c = 'D1', UnitId__c = u.Id, Size__c = 'STUDIO/1BA', Section_TypeId__c = sts.get('Studio').Id));
		insert uss.values();
	}
	@isTest static void testUpdateInventory() {
		Id resortId = [SELECT Id FROM Resort__c WHERE Name = 'Grand Pacific MarBrisa Resort'].Id;
		
		Test.startTest();
		ResortService.updateInventory(resortId, 'someone@email.com');
		Test.stopTest();

		Integer cntTotal = Database.countQuery('SELECT count() FROM Unit_Inventory__c');
		System.assert(cntTotal == 9, 'Incorrect unit inventories created');
	}
	

	
}