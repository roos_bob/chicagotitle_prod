/**
 * Auto Generated and Deployed by the Declarative Lookup Rollup Summaries Tool package (dlrs)
 **/
@IsTest
private class dlrs_Overlapping_Usage_TypeTest
{
    @IsTest
    private static void testTrigger()
    {
        // Force the dlrs_Overlapping_Usage_TypeTrigger to be invoked, fails the test if org config or other Apex code prevents this.
        dlrs.RollupService.testHandler(new Overlapping_Usage_Type__c());
    }
}