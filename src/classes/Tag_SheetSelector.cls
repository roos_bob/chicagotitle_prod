public with sharing class Tag_SheetSelector extends fflib_SObjectSelector
{
	public List<Schema.SObjectField> getSObjectFieldList()
	{
		return new List<Schema.SObjectField> {
			Tag_Sheet__c.Id,
			Tag_Sheet__c.Name
		};
	}
	
	public Schema.SObjectType getSObjectType()
	{
		return Tag_Sheet__c.sObjectType;
	}

	public List<Tag_Sheet__c> selectById(Set<ID> idSet)
	{
		return (List<Tag_Sheet__c>) selectSObjectsById(idSet);
	}

	public fflib_QueryFactory initNewQueryFactory() {
		fflib_QueryFactory query = newQueryFactory();
		return query;
	}

	public List<Tag_Sheet__c> selectByName(List<String> nameList)
	{
		fflib_QueryFactory query = initNewQueryFactory();
		query.setCondition('Name IN :nameList');
		return  (List<Tag_Sheet__c>) Database.query(query.toSOQL());
	}

	public Id selectIdByBatchName(string batchName) {
		fflib_QueryFactory query = initNewQueryFactory();
		query.setCondition('Deeded_Batch_HeaderId__r.Name = :batchName');
		Id objId = null;
		List<Tag_Sheet__c> objs = Database.query(query.toSOQL());
		if (objs.size() > 0) objId = objs[0].Id;
		return objId;

	}

}