/**
 * Created by bob.roos on 2/16/2017.
 */

public class Owner_List_BeforeInsert extends TriggerHandlerBase   {

    public override void mainEntry(TriggerParameters tp) {
        Owner_List_Helper.setContractIdFromTransaction(tp.newList);
    }


}