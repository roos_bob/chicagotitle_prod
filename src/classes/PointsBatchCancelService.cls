/*
Test Class: Contract_BatchCancel_Test
*/
global class PointsBatchCancelService
{
	webservice static void cancelContracts(string[] contractNames, Id developerId, Date closingDate, string comments)
	{
		List<string> userEmail = new List<String> { UserInfo.getUserEmail() };
        Integer batchSize = 5;
		Database.executeBatch(new Contract_BatchCancel(contractNames, developerId, closingDate, comments, userEmail), batchSize);
	}
}