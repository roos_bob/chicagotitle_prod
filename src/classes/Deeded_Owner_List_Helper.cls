public with sharing class Deeded_Owner_List_Helper {
	// Lookups to Contract and Unit Inventory were created on Owner List, despite them being redundant since they can
	// be derived from the Document master lookup.  The reason for this denormalization is allow for certain queries to
	// work when quering owners related to contracts and inventories.  Without the direct relationship, we would run into
	// soql limitations when it comes to nesting sub selects.
	// Note that this method assumes this is being called from before triggers so no explict saves are being done here.
	public static void setDocumentRelatedLookups(List<Deeded_Owner_List__c> olist) {
		Set<Id> docIds = new Set<Id>();
		for(Deeded_Owner_List__c o : olist) {
			docIds.add(o.Deeded_Document__c);
		}

		Map<Id, Deeded_Document__c> docMap = new Map<Id, Deeded_Document__c>(
			[SELECT Id, Deeded_Contract__c, Deeded_Contract__r.Unit_InventoryId__c 
			 FROM Deeded_Document__c
			 WHERE Id IN :docIds]);

		for(Deeded_Owner_List__c o : olist) {
			o.Deeded_Contract__c = docMap.get(o.Deeded_Document__c) == null ? null : docMap.get(o.Deeded_Document__c).Deeded_Contract__c;
			o.Unit_Inventory__c = docMap.get(o.Deeded_Document__c) == null ? null : docMap.get(o.Deeded_Document__c).Deeded_Contract__r.Unit_InventoryId__c;
		}
	}
}