public class Unit_Inventory_Helper {

	public static Map<Id, boolean> UnitInventoryIsAvailable(Set<Id> inventoryIds, string sellingEntity ) {
		Map<Id, boolean> retMap = new Map<Id, boolean>(); 
		List<Unit_Inventory__c> unitInventoryList = new Unit_InventorySelector().selectById(inventoryIds);
		for (Unit_Inventory__c inventory : unitInventoryList) {
			//retMap.put(inventory.Id, inventory.Blocked__c)
		}


		return retMap;

	}

	public class UnitInventoryAvailable {
		public boolean isBlocked;
		public boolean hasContract; 
		public boolean hasTrusteesDeed;
		public boolean hasDevGrantDeed;
		public boolean hasDeedOfTrust;
		public boolean hasReconveyance;
		public string unitSellingEntityName;
		public string proposedSellingEntityName;
		
		public boolean hasSellingEntity { get	{ return unitSellingEntityName != null; }}
		public boolean isSameSellingEntity { get { return unitSellingEntityName == proposedSellingEntityName; }	}

		public boolean UnitIsAvailable {
			get {
				if (isBlocked || (hasSellingEntity && !isSameSellingEntity)) {
					return false;
				} 
				if (!hasContract || hasTrusteesDeed) {
					return true;
				} 
				if (!hasDevGrantDeed) {
					return false;
				} 
				if(!hasDeedOfTrust || hasReconveyance) {
					return true;
				} 
				return false;			
			}
		}

		public UnitInventoryAvailable() {
			isBlocked = false;
			hasContract = false;
			hasTrusteesDeed = false;
			hasDevGrantDeed = false;
			hasDeedOfTrust = false;
			hasReconveyance = false;
			unitSellingEntityName = null;
			proposedSellingEntityName = null;
		}
	}
	
}