@isTest
private class Deeded_DocumentSelectorTest {

	@isTest
	private static void testSelector() {
		//string triggerSetting1 = '';
		//string triggerSetting2 = '';
 		//CS_Trigger_Setting__c newCS;
        //newCS = new CS_Trigger_Setting__c(Name=triggerSetting1, Enabled__c=false);
        //insert newCS;
		//newCS = new CS_Trigger_Setting__c(Name=triggerSetting2, Enabled__c=false);
        //insert newCS;

		Deeded_Contract__c contractRecord = new Deeded_Contract__c(Name = 'test Contract');
		insert contractRecord;

		Deeded_Document__c record = new Deeded_Document__c(Name = 'test', Deeded_Contract__c = contractRecord.Id);
		insert record;

		Deeded_DocumentSelector selector = new Deeded_DocumentSelector();
		Set<Id> idSet = new Set<Id> {record.Id};
		List<String> nameList = new List<String> {'test'};
		
		List<Deeded_Document__c> byID = selector.selectById(idSet);
		System.assert(byId.size() == 1);

		List<Deeded_Document__c> byName = selector.selectByName(nameList);
		System.assert(byName.size() == 1);
		


	}
}