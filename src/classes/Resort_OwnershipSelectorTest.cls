@isTest
private class Resort_OwnershipSelectorTest {

	@isTest
	private static void testSelector() {
		Resort__c resort = new Resort__c(Name='test', Code__c='XX');
		insert resort;
		Resort_Ownership__c record = new Resort_Ownership__c(Name = 'test', ResortId__c=resort.Id );
		insert record;
		record = [select Id, Name, Ownership_Code__c from Resort_Ownership__c];

		Resort_OwnershipSelector selector = new Resort_OwnershipSelector();
		Set<Id> idSet = new Set<Id> {record.Id};
		List<String> nameList = new List<String> {'test'};
		Set<String> nameSet = new Set<String> {'XXtest'};
		
		List<Resort_Ownership__c> byID = selector.selectById(idSet);
		System.assert(byId.size() == 1);

	
		Map<String, Resort_Ownership__c> mapByCode = selector.selectMapByCode(nameSet);
		System.assert(mapByCode.size() == 1);

	}
}