/**
* @author CRMCulture
* @version 1.00
* @description This maintains the states and provides functionality needed for paging 
*/
public class Pager {
	public Integer counter;
	private Integer listSize;
	public Integer totalSize {get;set;}

	/**
	* @author CRMCulture
	* @version 1.00
	* @description Constructor 
	*/
	public Pager(Integer listSize) {
		this.listSize = listSize;
		this.counter = 0;
      	this.totalSize = 0;
	}

	public PageReference goToBeginning() { 
   	counter = 0;
   	return null;
	}

	public PageReference goToPrevious() { 
   	counter -= listSize;
   	return null;
	}

	public PageReference goToNext() { 
   	counter += listSize;
   	return null;
	}

	public PageReference goToEnd() { 
   	if (Math.mod(totalSize, listSize) == 0) {
   		counter = ((totalSize / listSize) - 1) * listSize;
   	} else {
   		counter = (totalSize / listSize) * listSize;
   	}
   	return null;
	}

	public Boolean getDisablePrevious() { 
   	//this will disable the previous and beginning buttons
   	if (counter>0) return false; 
   	else return true;
  	}
 
	public Boolean getDisableNext() { 
		//this will disable the next and end buttons
 	if (counter + listSize < totalSize) return false; 
 	else return true;
	}

 
	public Integer getPageNumber() {
   	return counter/listSize + 1;
	}

	public Integer getTotalPages() {
		
   	if (math.mod(totalSize, listSize) > 0) {
      	return totalSize/listSize + 1;
   	} else {
      	return (totalSize/listSize);
   	}
	}

}