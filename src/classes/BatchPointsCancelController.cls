public class BatchPointsCancelController  {
	public string inputData {get; set;}
	public Date closingDate {get; set;}
	public List<SelectOption> developerSelectOptions {get; set;}
	public Id developerId {get; set;}
	public string comments {get; set;}

	public BatchPointsCancelController() {
		closingDate = Date.today();
		List<Developer__c > developers = [select Id, name from Developer__c where RecordType.Name = 'Points' order by Name];
		developerSelectOptions = new List<SelectOption>();
		developerId = null;
		for (Developer__c developer : developers) {
			if (developerId == null ) { developerId = developer.Id; }
			developerSelectOptions.add(new SelectOption(developer.Id, developer.Name));
		}
	}

}