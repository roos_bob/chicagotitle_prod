/**
 * @author Bob Roos (bob.roos@getConga.com)
 * @version 1.0.0
 * @description This page is used for Points Functions 1) Transfer 
 *
 **/
public class PointsContractFunctionTransferController {
	private ApexPages.StandardController stdController;

	public List<Transaction__c> originalTransactions { get; set; }
	public List<Owner__c> newOwnerList { get; set; }
	public List<Owner__c> cleanOwnerList { get; set; }
	public List<wrapOwner> oldOwnerList { get; set; }
	public List<wrapPointsContract> relatedContractList { get; set; }
	public List<wrapOwner> relatedOwnerList { get; set; }

	public Transaction__c newTrans { get; set; }
	public Owner__c editOwner { get; set; }
	public Contract__c originalContract { get; set; }

	private Id originalContractId;
	public Id lastOwnerTransId { get; set; }
	public Boolean showOwnerPopup { get; set; }
	public Boolean isCreateOwner { get; set; }
	public Boolean isEditOwner { get; set; }
	public Boolean isTransfer { get; set; }
	public Boolean isChange { get; set; }
	public Boolean isAddRemove { get; set; }
	public Boolean isApplyToAllContracts { get; set; }
	public Boolean displayRelatedContracts { get; set; }
	public Boolean displayRelatedOwners { get; set; }
	public Boolean allowSave { get; set; }
	public integer ownerRowNumber { get; set; }
	public string pageAction { get; set; }
	public string pageTitle { get; set; }

	public string membershipNumber { get; set; }
	public string contractNumber { get; set; }
	/*Constructor*/
	public PointsContractFunctionTransferController(ApexPages.StandardController stdController) {
		System.debug('Controller init - has parameters?: ' + ApexPages.currentPage().getParameters().get('contractId'));
		if (ApexPages.currentPage().getParameters().get('contractId') != null) {
			this.stdController = stdController;
			init();
		}
	}

	public void init() {
		System.debug('Called Init method');
		this.newTrans = (Transaction__c) stdController.getRecord();
		originalContractId = ApexPages.currentPage().getParameters().get('contractId');
		// pageAction can be one of : transfer, addremove, change
		pageAction = ApexPages.currentPage().getParameters().get('action');
		isTransfer = pageAction == 'transfer'; // Transfer Ownership
		isAddRemove = pageAction == 'addremove'; // Add Remove Owners
		isChange = pageAction == 'change'; // Owner Name Change
		newOwnerList = new List<Owner__c> ();
		newTrans.ContractId__c = originalContractId;
		showOwnerPopup = false;
		isCreateOwner = false;
		isEditOwner = false;
		isApplyToAllContracts = false;
		displayRelatedContracts = false;
		allowSave = true;

		originalContract = new ContractSelector().getContract(originalContractId);
		lastOwnerTransId = originalContract.Last_Owner_Trans__c;

		List<Owner_List__c> ol = Contract_Helper.selectOwnerList(lastOwnerTransId);
		List<Owner__c> owners = Contract_Helper.selectOwners(lastOwnerTransId);

		if (isTransfer || isAddRemove) {
			List<Contract__c> contracts = Contract_Helper.selectRelatedContracts(originalContractId);
			relatedContractList = new List<wrapPointsContract> ();
			for (Contract__c contract : contracts) {
				relatedContractList.add(new wrapPointsContract(contract));
				isApplyToAllContracts = true;
				displayRelatedContracts = true;
			}
		}


		// TRANSFER:  Remove all current owners, add new owners
		if (isTransfer) {
			//Show Contract context
			//Show Date and Comment prompts

			//Show New Owners grid
			pageTitle = 'Transfer Ownership';

		}
		// ADDREMOVE: Keep at least one owner, add/remove others
		if (isAddRemove) {
			//Show Contract context
			//Show Date and Comment prompts

			//Show checkbox list of current owners ("REMOVE", remove selected items)
			//Show New Owners grid
			pageTitle = 'Add Remove Owners';
			oldOwnerList = new List<wrapOwner> ();
			for (Owner__c owner : owners) {
				oldOwnerList.add(new wrapOwner(owner));
			}
			//newOwnerList  is an empty list

		}
		// CHANGE: Keep all owners, change name(s)
		if (isChange) {
			//Show Contract context
			//Show Date and Comment prompts
			//Hide New Owners grid
			// Show editable list of all owners
			newOwnerList = owners;
			cleanOwnerList = owners.deepClone(true);
			pageTitle = 'Owner Name Change';
			relatedOwnerList = new List<wrapOwner> ();
			relatedOwnerList = Contract_Helper.selectOwnersForMembership(originalContract.Membership_Number__c, originalContractId);
			//if (relatedOwnerList.size() > 0) { displayRelatedOwners = true; }
			allowSave = false;
		}
	}


	public void addOwnerRowAction() {
		isCreateOwner = true;
		Owner__c o = new Owner__c();
		o.Developer__c = originalContract.Membership_Number__r.Developer__c;
		newOwnerList.add(o);
		editOwner = o;
		showOwnerPopup = true;
	}

	public void editOwnerRowAction() {
		isEditOwner = true;
		showOwnerPopup = true;
		editOwner = newOwnerlist.get(ownerRowNumber - 1);
	}
	public void deleteOwnerRowAction() {
		showOwnerPopup = false;
		newOwnerList.remove(ownerRowNumber - 1);
	}

	public void saveNewOwnerPageAction() {
		showOwnerPopup = false;
		isCreateOwner = false;
		isEditOwner = false;

		if (isChange) {
			integer dirtyOwners = Contract_Helper.countDirtyOwners(newOwnerList, cleanOwnerList);
			displayRelatedOwners = relatedOwnerList.size() > 0 && dirtyOwners == 1;
			allowSave = dirtyOwners > 0;
		}

	}
	public void hideNewOwnerPageAction() {
		showOwnerPopup = false;
		if (isCreateOwner == true) {
			Integer sz = newOwnerList.size() - 1;
			newOwnerList.remove(sz);
		}
		isCreateOwner = false;
		isEditOwner = false;

	}
	public void hideRelatedContractsSectionOnChange() {
		if (!String.isBlank(membershipNumber) && !String.isBlank(contractNumber)) {
			displayRelatedContracts = false;
		} else {
			displayRelatedContracts = isApplyToAllContracts;
		}
	}

	public PageReference saveAndReturn()
	{
		//stdController.save();
		if (pageAction == 'transfer') {
			if (String.isBlank(membershipNumber) || String.isBlank(contractNumber)) {
				Contract_Helper.newTransferTransaction(originalContractId, newTrans.Closing_Date__c, newTrans.Comments__c, newOwnerList, relatedContractList);
			} else {
				Contract_Helper.newSplitTransaction(originalContractId, membershipNumber, contractNumber, newTrans.Closing_Date__c, newTrans.Comments__c, 0, originalContract.Points__c, newOwnerList);
			}
		}
		if (pageAction == 'addremove') {
			Contract_Helper.newAddRemoveTransaction(originalContractId, newTrans.Closing_Date__c, newTrans.Comments__c, newOwnerList, oldOwnerList, relatedContractList);
		}
		if (pageAction == 'change') {
			Contract_Helper.newNameChangeTransaction(originalContractId, newTrans.Closing_Date__c, newTrans.Comments__c, newOwnerList, cleanOwnerList, relatedOwnerList);
		}
		PageReference parentPage = new PageReference('/' + originalContractId);
		parentPage.setRedirect(true);
		return parentPage;
	}



}