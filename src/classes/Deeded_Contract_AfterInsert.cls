/** 
* Deeded_Contract_AfterInsert  Trigger Handler
*
* @author CRMCulture 
* @version 1.00
* @description  Class to handle Trigger handler
* @return void
*/
public class Deeded_Contract_AfterInsert extends TriggerHandlerBase {
    /** 
    * mainEntry override
    *
    * @author CRMCulture
    * @date 20140701
    * @version 1.00
    * @description mainEntry override
    * @param tp Trigger Parameters construct
    * @return void
    */
    public override void mainEntry(TriggerParameters tp) {
    	// Create Unit Inventory Contract join records to represent Unit Inventories that
    	// are made unavailable because they overlap or conflict with the Unit Inventory on the contract.
    	if (TriggerHelper.DoExecute('Deeded_Contract__c.UpdateUIC')) {
    		Deeded_Contract_Helper.updateUnitInventoryContract(tp.newList);
    	}

        // Creates a title policy for each contract created.
        if (TriggerHelper.DoExecute('Deeded_Contract__c.createTitlePolicies')) {
            Deeded_Contract_Helper.createTitlePolicies(tp.newList);
        }
    }
    /** 
    * @author CRMCulture
    * @version 1.00, 20140701
    * @description Called for the subsequent times in the same execution context. The trigger handlers can chose
    *               to ignore if they don't need the reentrant feature.
    * @param TriggerParameters The trigger parameters such as the list of records before and after the update.
    */
    //public override void inProgressEntry(TriggerParameters tp) {
        
    //}
}