global class sObjectsBatchDelete implements Database.Batchable<SObject> {
	
	global final String Query;
    global sObjectsBatchDelete(String q){
        Query=q;
    }

    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }

   	global void execute(Database.BatchableContext context, List<SObject> scope) {
		delete scope;
	}

	global void finish(Database.BatchableContext context) {
		
	}
}