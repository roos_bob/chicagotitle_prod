@isTest
public class wrapOwner_Test {
	static testMethod void validateSelected() {
		Owner__c o = new Owner__c();
		o.Last_Name__c = 'Lastname';
		insert o;
		wrapOwner wo = new wrapOwner(o);
		System.assert(wo.selected == false);
	}
}