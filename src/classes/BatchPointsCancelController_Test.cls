@isTest
private class BatchPointsCancelController_Test {


	@testSetup static void setupTestData() {
	List<SObjectType> uowObjs = new List<SObjectType> { Developer__c.SObjectType, Resort__c.SObjectType, Transaction_Type__c.SObjectType, Membership_Number__c.SObjectType, Contract__c.SObjectType, Transaction__c.SObjectType, Owner__c.SObjectType, Owner_List__c.SObjectType };
		fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(uowObjs);
		Id pointsRecordType = Schema.SObjectType.Developer__c.getRecordTypeInfosByName().get('Points').getRecordTypeId();
		List<Developer__c> developer = (List<Developer__c>) TestingUtility.createSObjectList('Developer__c', 1, false, false);
		developer[0].RecordTypeId = pointsRecordType;
		uow.registerNew(developer[0]);
		List<Resort__c> resorts = (List<Resort__c>) TestingUtility.createSObjectList('Resort__c', 1, false, false);
		uow.registerNew(resorts[0], Resort__c.Developer__c, developer[0]);

		List<Membership_Number__c> membershipNumbers = (List<Membership_Number__c>) TestingUtility.createSObjectList('Membership_Number__c', 1, false, false);
		uow.registerNew(membershipNumbers[0]);

		List<Contract__c> contracts = (List<Contract__c>) TestingUtility.createSObjectList('Contract__c', 1, false, false);
		uow.registerNew(contracts[0], Contract__c.Membership_Number__c, membershipNumbers[0]);
		uow.registerRelationship(contracts[0], Contract__c.Resort__c, resorts[0]);

		List<Transaction_Type__c> transactionTypes = (List<Transaction_Type__c>) TestingUtility.createSObjectList('Transaction_Type__c', 6, false, false);
		transactionTypes[0].Name = 'Issued';
		transactionTypes[0].Ownership__c = true;

		transactionTypes[1].Name = 'Cancelled';
		transactionTypes[1].Ownership__c = false;
		transactionTypes[1].Points__c = true;

		transactionTypes[2].Name = 'Split';
		transactionTypes[2].Ownership__c = true;
		transactionTypes[2].Points__c = true;

		transactionTypes[3].Name = 'Transferred';
		transactionTypes[3].Ownership__c = true;
		transactionTypes[3].Points__c = false;

		transactionTypes[4].Name = 'Add/Remove Owner';
		transactionTypes[4].Ownership__c = true;
		transactionTypes[4].Points__c = false;

		transactionTypes[5].Name = 'Name Change';
		transactionTypes[5].Ownership__c = true;
		transactionTypes[5].Points__c = false;

		uow.registerNew(transactionTypes);

		List<Transaction__c> transactions = (List<Transaction__c>) TestingUtility.createSObjectList('Transaction__c', 1, false, false);
		transactions[0].Closing_Date__c = Date.today();
		transactions[0].Name = 'test';
		uow.registerNew(transactions[0], Transaction__c.ContractId__c, contracts[0]);
		uow.registerRelationship(transactions[0], Transaction__c.Transaction_Type__c, transactionTypes[0]);

		//transactions = [select id, Name, ContractId__c, Closing_Date__c, Transaction_Name__c from Transaction__c];

		List<Owner__c> owners = (List<Owner__c>) TestingUtility.createSObjectList('Owner__c', 3, false, false);
		integer i = 0;
		for (Owner__c owner : owners) {
			owner.Last_Name__c = 'Test ' + i++;
			owner.First_Name__c = 'Firstname';
		}
		uow.registerNew(owners);
		for (Owner__c owner : owners) {
			Owner_List__c ol = new Owner_List__c();
			uow.registerNew(ol, Owner_List__c.Transaction__c, transactions[0]);
			uow.registerRelationship(ol, Owner_List__c.Owner__c, owner);
		}
		uow.commitWork();
	}

	@isTest
	private static void testName() {
			Test.startTest();
			PageReference pr = Page.BatchPointsCancel;
			Test.setCurrentPage(pr);
			BatchPointsCancelController p = new BatchPointsCancelController();
			p.inputData = 'test';
			p.comments = 'test';
			Test.stopTest();
	}
}