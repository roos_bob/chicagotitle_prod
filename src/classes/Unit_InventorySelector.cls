public with sharing class Unit_InventorySelector extends fflib_SObjectSelector
{
	public List<Schema.SObjectField> getSObjectFieldList()
	{
		return new List<Schema.SObjectField> {
			Unit_Inventory__c.Id,
			Unit_Inventory__c.Name,
			Unit_Inventory__c.Code__c,
			Unit_Inventory__c.ResortId__c,
			Unit_Inventory__c.Occupied__c,
			Unit_Inventory__c.Blocked__c, 
			Unit_Inventory__c.Deeded_Selling_Entity__c
		};
	}
	
	public Schema.SObjectType getSObjectType()
	{
		return Unit_Inventory__c.sObjectType;
	}

	public List<Unit_Inventory__c> selectById(Set<ID> idSet)
	{
		return (List<Unit_Inventory__c>) selectSObjectsById(idSet);
	}

	public fflib_QueryFactory initNewQueryFactory() {
		fflib_QueryFactory query = newQueryFactory();
		return query;
	}

	public List<Unit_Inventory__c> selectByName(List<String> nameList)
	{
		fflib_QueryFactory query = initNewQueryFactory();
		query.setCondition('Name IN :nameList');
		return  (List<Unit_Inventory__c>) Database.query(query.toSOQL());
	}


	public Map<String, Unit_Inventory__c> selectMapByICN(Set<String> codes)
	{
		fflib_QueryFactory query = initNewQueryFactory();
		query.setCondition('Code__c IN :codes');
		List<Unit_Inventory__c> records = (List<Unit_Inventory__c>) Database.query(query.toSOQL());

		Map<String, Unit_Inventory__c> resultMap = new Map<String, Unit_Inventory__c>();
		for (Unit_Inventory__c record: records) {
			resultMap.put(record.Code__c, record);
		}
		return resultMap;
	}


}