public class DeededTitlePolicyArchiveController {
	public Id contractId { get; set; }
	public Deeded_Contract__c contract { get; set; }

	//constructor to get the Case record
	public DeededTitlePolicyArchiveController(ApexPages.StandardController controller) {
		contract = (Deeded_Contract__c) controller.getRecord();
		contractId = contract.Id;
	}

	//Method that can is called from the Visual Force page action attribute
	public PageReference archiveTitlePolicy() {
		
		Deeded_Contract_Helper.archiveTitlePolicy(contractId);

		PageReference pageRef = new PageReference('/' + contractId);
		pageRef.setRedirect(true);
		return pageRef; //Returns to the Contract page
	}

}