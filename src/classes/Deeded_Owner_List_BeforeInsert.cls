/** 
* Deeded_Owner_List_BeforeInsert  Trigger Handler
*
* @author CRMCulture 
* @version 1.00
* @description  Class to handle Trigger handler
* @return void
*/
public with sharing class Deeded_Owner_List_BeforeInsert extends TriggerHandlerBase {
    /** 
    * mainEntry override
    *
    * @author CRMCulture
    * @date 20140701
    * @version 1.00
    * @description mainEntry override
    * @param tp Trigger Parameters construct
    * @return void
    */
    public override void mainEntry(TriggerParameters tp) {
        if (TriggerHelper.DoExecute('Deeded_Owner_List__c.setLookups')) {
        	Deeded_Owner_List_Helper.setDocumentRelatedLookups(tp.newList);
        }
    }
    /** 
    * @author CRMCulture
    * @version 1.00, 20140701
    * @description Called for the subsequent times in the same execution context. The trigger handlers can chose
    *               to ignore if they don't need the reentrant feature.
    * @param TriggerParameters The trigger parameters such as the list of records before and after the update.
    */
    //public override void inProgressEntry(TriggerParameters tp) {
        
    //}	
}