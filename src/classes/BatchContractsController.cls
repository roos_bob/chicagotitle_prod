public class BatchContractsController {
	public string inputType {get; set;}
	public string inputData {get; set;}
	public Date fDate {get; set;}


	public BatchContractsController() {
		fDate = Date.today();
	}


	@RemoteAction
	public static List<Object> getGrandPacificValidationData (List<Deeded_Contract_Helper.ImportedContractRecord> records) {
		return Deeded_Contract_Helper.getGrandPacificValidationData(records);
	}


	@RemoteAction
	public static Map<String, Object> loadData(List<Deeded_Batch_Detail__c> dataRows, List<Deeded_Batch_Detail_Owner__c> ownerRows, string batchType, string batchId, string developerId ) {
		Map<String, Object> retVal = Deeded_Contract_Helper.loadBatchData(dataRows, ownerRows, batchType, batchId, developerId);
		System.debug(retVal);
		return retVal;

	}


}