@isTest
private class ResortSelectorTest {

	@isTest
	private static void testSelector() {
		Resort__c record = new Resort__c(Name = 'test');
		insert record;

		ResortSelector selector = new ResortSelector();
		Set<Id> idSet = new Set<Id> {record.Id};
		List<String> nameList = new List<String> {'test'};
		Set<String> nameSet = new Set<String> {'test'};
		
		List<Resort__c> byID = selector.selectById(idSet);
		System.assert(byId.size() == 1);

		List<Resort__c> byName = selector.selectByName(nameList);
		System.assert(byName.size() == 1);

		List<Resort__c> byNameSet = selector.selectByName(nameSet);
		System.assert(byNameSet.size() == 1);
		
		Map<String, Resort__c> mapByName = selector.selectMapByName(nameSet);
		System.assert(mapByName.size() == 1);

	}
}