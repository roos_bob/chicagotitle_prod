@isTest
private class ContractSelectorTest {
   @testSetup static void setupTestData(){
		List<Developer__c> developer = (List<Developer__c>)TestingUtility.createSObjectList('Developer__c', 1, false, false);
		insert developer;
        List<Resort__c> resorts = (List<Resort__c>)TestingUtility.createSObjectList('Resort__c', 1, false, false);
		resorts[0].Developer__c = developer[0].Id;
        insert resorts;
        List<Membership_Number__c> membershipNumbers = (List<Membership_Number__c>)TestingUtility.createSObjectList('Membership_Number__c', 1, false, false);
        insert membershipNumbers;
        List<Contract__c> contracts = (List<Contract__c>)TestingUtility.createSObjectList('Contract__c', 1, false, false);
        contracts[0].Membership_Number__c = membershipNumbers[0].Id;
        contracts[0].Resort__c = resorts[0].Id;
		contracts[0].Name = 'test';
        insert contracts;
		List<Transaction_Type__c> transactionTypes = (List<Transaction_Type__c>)TestingUtility.createSObjectList('Transaction_Type__c', 6, false, false);
		transactionTypes[0].Name = 'Issued';
		transactionTypes[0].Ownership__c = true;
		//transactionTypes[0].Developer__c = developer[0].Id;
		transactionTypes[1].Name = 'Cancelled';
		transactionTypes[1].Ownership__c = false;
		transactionTypes[1].Points__c = true;
		//transactionTypes[1].Developer__c = developer[0].Id;
		transactionTypes[2].Name = 'Split';
		transactionTypes[2].Ownership__c = true;
		transactionTypes[2].Points__c = true;
		//transactionTypes[2].Developer__c = developer[0].Id;
		transactionTypes[3].Name = 'Transferred';
		transactionTypes[3].Ownership__c = true;
		transactionTypes[3].Points__c = false;
		//transactionTypes[3].Developer__c = developer[0].Id;
		transactionTypes[4].Name = 'Add/Remove Owner';
		transactionTypes[4].Ownership__c = true;
		transactionTypes[4].Points__c = false;
		//transactionTypes[4].Developer__c = developer[0].Id;
		transactionTypes[5].Name = 'Name Change';
		transactionTypes[5].Ownership__c = true;
		transactionTypes[5].Points__c = false;
		//transactionTypes[5].Developer__c = developer[0].Id;

		insert transactionTypes;
        List<Transaction__c> transactions = (List<Transaction__c>)TestingUtility.createSObjectList('Transaction__c', 1, false, false);
        transactions[0].ContractId__c = contracts[0].Id;
		transactions[0].Transaction_Type__c = transactionTypes[0].Id;
		transactions[0].Closing_Date__c = Date.today();
		transactions[0].Name = 'test';
        insert transactions;

		transactions = [select id, Name, ContractId__c, Closing_Date__c, Transaction_Name__c from Transaction__c];


        List<Owner__c> owners = (List<Owner__c>)TestingUtility.createSObjectList('Owner__c', 3, false, false);
		integer i = 0;
		for (Owner__c owner : owners) {
			owner.Last_Name__c = 'Test ' + i++;
			owner.First_Name__c = 'Firstname';
		}
        insert owners;
        List<Owner_List__c> ownerLists = new List<Owner_List__c>();
		ownerLists.add(new Owner_List__c());
		ownerLists.add(new Owner_List__c());
		ownerLists.add(new Owner_List__c());
		ownerLists[0].Transaction__c = transactions[0].Id;
		ownerLists[1].Transaction__c = transactions[0].Id;
		ownerLists[2].Transaction__c = transactions[0].Id;

		ownerLists[0].Owner__c = owners[0].Id;
		ownerLists[1].Owner__c = owners[1].Id;
		ownerLists[2].Owner__c = owners[2].Id;
		insert ownerLists;
    }

	@isTest
	private static void testSelector() {
		//Contract__c record = new Contract__c(Name = 'test');
		//insert record;

		ContractSelector selector = new ContractSelector();
		Set<Id> idSet = new Set<Id> {[select Id from Contract__c limit 1].Id};
		Set<String> nameSet = new Set<String> {'test'};
		
		List<Contract__c> byID = selector.selectById(idSet);
		System.assert(byId.size() == 1);

		Map<Id, Contract__c> mapById = selector.selectMapById(idSet);
		System.assert(mapById.size() == 1);

		List<Contract__c> byName = selector.selectByName(nameSet);
		System.assert(byName.size() == 1);

		List<Contract__c> byNameSet = selector.selectByName(nameSet);
		System.assert(byNameSet.size() == 1);
		
		Map<String, Contract__c> mapByName = selector.selectMapByName(nameSet);
		System.assert(mapByName.size() == 1);

	}
}