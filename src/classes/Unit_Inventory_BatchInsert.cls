/** 
* @author CRMCulture 
* @version 1.00
* @description  Batch class to create unit inventories for a given resort.  It processes units of the given resort in batches to
* avoid hitting governor limits.  Note that we're batching up units instead of unit sections to make sure that all sections within
* a unit are processed in the same batch so that we can resolve the ids of overlapping inventories in the same batch.
* @return void
*/
global class Unit_Inventory_BatchInsert implements Database.Batchable<SObject>, Database.Stateful {
	
	public Id resortId;
	public List<Usage_Type__c> usageTypes;
	public Map<Id, Section_Type__c> sectionTypeMap;
	public List<Week__c> weeks;
	public String userEmail;
	public Integer numberOfUnitInventoriesCreated = 0;

	global Unit_Inventory_BatchInsert(Id resortId) {
		this(resortId, UserInfo.getUserEmail());
	}

	global Unit_Inventory_BatchInsert(Id resortId, String userEmail) {
		this.resortId = resortId;
		this.userEmail = userEmail;

		// These are reference data that will be needed for each batch processsing so we initialize these once in the constructor
		// and make them available to the batches.
		this.usageTypes = [SELECT Id, ICN_Code__c, (SELECT Overlaps_WithId__c FROM Overlapping_Usage_Types__r) FROM Usage_Type__c WHERE Resort__c = :this.resortId];
		this.sectionTypeMap = new Map<Id, Section_Type__c>([SELECT Id, (SELECT Overlaps_WithId__c FROM Overlapping_Section_Types__r) FROM Section_Type__c WHERE ResortId__c = :this.resortId]);
		this.weeks = [SELECT Id, ICN_Code__c FROM Week__c WHERE Resort__c = :this.resortId];
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator([SELECT Id, ICN_Code__c, ResortId__r.Code__c, (SELECT Id, ICN_Code__c, Section_TypeId__c, Section_TypeId__r.Name FROM Unit_Sections__r) FROM Unit__c WHERE ResortId__c = :this.resortId]);
	}

	global void execute(Database.BatchableContext context, List<SObject> scope) {
		try {
			// Map of unit inventory by key: <Unit Id> <Section Type Id> <Usage Type Id> <Week Id>
			Map<String, Unit_Inventory__c> uiMap = new Map<String, Unit_Inventory__c>();

			// Map of Overlapping Unit Inventory Keys: inventory key to list of overlapping inventory keys
			// Example: If the inventory is for Full, Annual, Week 1, and assuming the unit has  
			// Bedroom and Studio sections, this map entry for full, annual, week 1 will have a list containing the
			// following inventories:
			// 1. bedroom, even, week 1
			// 2. studio, even, week 1
			// 3. bedroom, odd, week 1
			// 4. studio, odd, week 1
			// This means that if full, annual, week 1 is sold, these 4 other inventories would become unavailable (occupied),
			// even though the contract is only directly linked to full, anual, week 1.
			Map<String, List<String>> overlappingUIKeyByUIKey = new Map<String, List<String>>();

			// Create unit inventories for each unit section, usage, and week.
			// Also, determine the overlapping inventories for each inventory.
			for(Unit__c unit : (List<Unit__c>)scope) {
				for(Unit_Section__c unitSection : unit.Unit_Sections__r) {
					for(Usage_Type__c usageType : usageTypes) {
						for(Week__c week : weeks) {
							String uiKey = unit.Id + ' ' + unitSection.Section_TypeId__c + ' ' + usageType.Id + ' ' + week.Id;
							
							Unit_Inventory__c ui = new Unit_Inventory__c();
							//ui.Name = unit.ResortId__r.Code__c + unit.ICN_Code__c + week.ICN_Code__c + unitSection.ICN_Code__c +  + usageType.ICN_Code__c;
							ui.Name = unit.ICN_Code__c + week.ICN_Code__c + unitSection.ICN_Code__c +  + usageType.ICN_Code__c;
							ui.ResortId__c = this.resortId;
							ui.Unit_SectionId__c = unitSection.Id;
							ui.Usage_TypeId__c = usageType.Id;
							ui.WeekId__c = week.Id;
							ui.Unique_Key__c = uiKey;
							
							uiMap.put(uiKey, ui);

							List<String> overlapKeys = new List<String>();

							// The goal is to have a comma delimited list of overlapping inventory id, but since the inventories don't 
							// exist yet until we perform the insert later, we'll have to keep track of the association of the inventory to its
							// list of overlapping inventories using keys.  Then after the insert, we'll resolve the keys to their corresponding
							// id's and update the Overlaps_With__c field on the unit inventory.
							for(Overlapping_Usage_Type__c out : usageType.Overlapping_Usage_Types__r) {
								for(Overlapping_Section_Type__c ost : sectionTypeMap.get(unitSection.Section_TypeId__c).Overlapping_Section_Types__r) {
									String overlapUIKey = unit.Id + ' ' +  ost.Overlaps_WithId__c + ' ' + out.Overlaps_WithId__c + ' ' + week.Id;
									overlapKeys.add(overLapUIKey);
								}
							}
							overlappingUIKeyByUIKey.put(uiKey, overLapKeys);
						}
					}
				}
			}
			
			// Since this batch could be run again for the same resort in case there are new units being added, it's possible
			// for there to be existing inventory.  So an upsert using the unique key should take care of not creating dupes.
			for(Database.UpsertResult ur : Database.upsert(uiMap.values(), Unit_Inventory__c.Fields.Unique_Key__c, false)) {
				if (ur.isCreated()) numberOfUnitInventoriesCreated++;
			}

			// Resolve the overlapping inventory keys to form the comma delimited list of overlapping inventory ids.
			for(String uiKey : uiMap.keySet()) {
				Unit_Inventory__c ui = uiMap.get(uiKey);
				List<String> overlappingUIIds = new List<String>();
				for(String overlapKey : overlappingUIKeyByUIKey.get(uiKey)) {
					// Not all potential overlaps exist.  For example, a unit may only have a full section (maybe because it's a studio).
					// So although a full could potentially overlap with a bedroom section or studio section, in this case, it doesn't
					// because the section doesn't exist.  
					if (uiMap.containsKey(overLapKey)) {
						overlappingUIIds.add(uiMap.get(overLapKey).Id);
					}
				}

				ui.Overlaps_With__c = String.join(overlappingUIIds, ',');
			}

			update uiMap.values();


		} catch(Exception exc) {
			System_Issue_Log_Helper.LogException(new SystemIssueLogWrapper('Unit_Inventory_BatchInsert', 'execute', new List<Id> {context.getJobId()}, exc ) ,true);
		}
	}
	
	
	
	/**
	 * @description gets invoked when the batch job finishes. Place any clean up code in this method.
	 * @param context contains the job ID
	 */ 
	global void finish(Database.BatchableContext context) {
		String resortName = [SELECT Name FROM Resort__c WHERE Id = :resortId].Name;
		List<String> recipients = new List<String>{ userEmail };
		String subject = String.format('Inventory update for {0} is done.', new String[] { resortName });
		String body = String.format('Inventory update for {0} is done.  {1} unit inventories were created.',
			new String[]{ resortName, String.valueOf(numberOfUnitInventoriesCreated) });

        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(recipients);
        mail.setSubject(subject);
        mail.setHtmlBody(body);

        mails.add(mail);
        
        Messaging.sendEmail(mails);
        
	}
}