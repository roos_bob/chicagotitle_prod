/**
 * Created by bob.roos on 2/16/2017.
 */

@isTest
public with sharing class Owner_List_HelperTest {

    @testSetup static void setupTestData(){
        List<Resort__c> resorts = (List<Resort__c>)TestingUtility.createSObjectList('Resort__c', 1, false, false);
        insert resorts;
        List<Membership_Number__c> membershipNumbers = (List<Membership_Number__c>)TestingUtility.createSObjectList('Membership_Number__c', 1, false, false);
        insert membershipNumbers;
        List<Contract__c> contracts = (List<Contract__c>)TestingUtility.createSObjectList('Contract__c', 1, false, false);
        contracts[0].Membership_Number__c = membershipNumbers[0].Id;
        contracts[0].Resort__c = resorts[0].Id;
        insert contracts;
		List<Transaction_Type__c> transactionTypes = (List<Transaction_Type__c>)TestingUtility.createSObjectList('Transaction_Type__c', 1, false, false);
		transactionTypes[0].Name = 'Issued';
		transactionTypes[0].Ownership__c = true;
		insert transactionTypes;
        List<Transaction__c> transactions = (List<Transaction__c>)TestingUtility.createSObjectList('Transaction__c', 1, false, false);
        transactions[0].ContractId__c = contracts[0].Id;
		transactions[0].Transaction_Type__c = transactionTypes[0].Id;
        insert transactions;
        transactions = (List<Transaction__c>)TestingUtility.createSObjectList('Transaction__c', 1, false, false);
        transactions[0].ContractId__c = contracts[0].Id;
		transactions[0].Transaction_Type__c = transactionTypes[0].Id;
        insert transactions;

        List<Owner__c> owners = (List<Owner__c>)TestingUtility.createSObjectList('Owner__c', 1, false, false);
		integer i = 0;
		for (Owner__c owner : owners) {
			owner.Last_Name__c = 'Test ' + i++;
			owner.First_Name__c = 'Firstname';
		}
        insert owners;
        //List<Owner_List__c> ownerLists = (List<Owner_List__c>)TestingUtility.createSObjectList('Owner_List__c', 1, false, false);
    }

    public static testMethod void testInsertOwnerList() {
        Test.startTest();
        List<Owner__c> owners = [SELECT Id FROM Owner__c];
        List<Transaction__c> transactions = [select Id, ContractId__c from Transaction__c];

        Owner_List__c ownerList = new Owner_List__c();
        ownerList.Transaction__c = transactions[0].id;
        ownerList.Owner__c = owners[0].id;
        insert ownerList;
        ownerList.Points_Id__c = 'test';
        update ownerList;

        List<Owner_List__c> ols = [select id, Contract__c from Owner_List__c];


        System.assert(transactions[0].ContractId__c != null);
        System.assert(ols[0].Contract__c != null);
        System.assert(ols[0].Contract__c == transactions[0].ContractId__c);

        Test.stopTest();
    }

    public static testMethod void testDeleteOwnerList() {
        Test.startTest();
		List<Owner__c> owners = [SELECT Id FROM Owner__c];
        List<Transaction__c> transactions = [select Id, ContractId__c from Transaction__c];

        Owner_List__c ownerList = new Owner_List__c();
        ownerList.Transaction__c = transactions[0].id;
        ownerList.Owner__c = owners[0].id;
        insert ownerList;

        List<Owner_List__c> olList = [SELECT Id FROM Owner_List__c];
		System.assert(olList.size() > 0);
		delete olList;
		List<Owner_List__c> deletedOwners = [SELECT Id FROM Owner_List__c];
        System.assert(deletedOwners.size() == 0);
        Test.stopTest();
    }
    public static testMethod void testDeleteTransaction() {
        Test.startTest();
		List<Owner__c> owners = [SELECT Id FROM Owner__c];
        List<Transaction__c> transactions = [select Id, ContractId__c from Transaction__c];

		System.assert(transactions.size() > 0);
		delete transactions;
		List<Transaction__c> deletedtrans = [select Id, ContractId__c from Transaction__c];
        System.assert(deletedtrans.size() == 0);
		UtilityClass.NewGuid();
        Test.stopTest();
    }

}