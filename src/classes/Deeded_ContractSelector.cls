public with sharing class Deeded_ContractSelector extends fflib_SObjectSelector
{
	public List<Schema.SObjectField> getSObjectFieldList()
	{
		return new List<Schema.SObjectField> {
			Deeded_Contract__c.Id,
			Deeded_Contract__c.Name
			//Deeded_Contract__c.Resort__c,
			//Deeded_Contract__c.Deeded_Base_Unit__c,
			//Deeded_Contract__c.Unit__c
		};
	}
	
	public Schema.SObjectType getSObjectType()
	{
		return Deeded_Contract__c.sObjectType;
	}

	public List<Deeded_Contract__c> selectById(Set<ID> idSet)
	{
		return (List<Deeded_Contract__c>) selectSObjectsById(idSet);
	}

	public fflib_QueryFactory initNewQueryFactory() {
		fflib_QueryFactory query = newQueryFactory();
		return query;
	}

	public List<Deeded_Contract__c> selectByName(List<String> nameList)
	{
		fflib_QueryFactory query = initNewQueryFactory();
		query.setCondition('Name IN :nameList');
		return  (List<Deeded_Contract__c>) Database.query(query.toSOQL());
	}

	public Map<String, Deeded_Contract__c> selectMapByName(Set<String> names)
	{
		fflib_QueryFactory query = initNewQueryFactory();
		query.setCondition('Name IN :names');
		List<Deeded_Contract__c> records = (List<Deeded_Contract__c>) Database.query(query.toSOQL());

		Map<String, Deeded_Contract__c> resultMap = new Map<String, Deeded_Contract__c>();
		for (Deeded_Contract__c record: records) {
			resultMap.put(record.Name, record);
		}
		return resultMap;

	}

}