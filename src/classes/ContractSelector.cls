public with sharing class ContractSelector extends fflib_SObjectSelector
{
	public List<Schema.SObjectField> getSObjectFieldList()
	{
		return new List<Schema.SObjectField> {
			Contract__c.Id,
			Contract__c.Name,
			Contract__c.Last_Owner_Trans__c,
			Contract__c.Owners__c
		};
	}
	
	public Schema.SObjectType getSObjectType()
	{
		return Contract__c.sObjectType;
	}

	public fflib_QueryFactory initNewQueryFactory() {
		fflib_QueryFactory query = newQueryFactory();
		return query;
	}

	public List<Contract__c> selectById(Set<ID> idSet)
	{
		return (List<Contract__c>) selectSObjectsById(idSet);
	}

	public Map<Id,Contract__c> selectMapById(Set<Id> idSet) {
		return new Map<Id, Contract__c>(selectById(idSet));
	}

	public List<Contract__c> selectByName(Set<String> names)
	{
		fflib_QueryFactory query = initNewQueryFactory();
		query.setCondition('Name IN :names');
		//query.selectField('Last_Owner_Trans__c');
		return  (List<Contract__c>) Database.query(query.toSOQL());
	}


	public Map<String, Contract__c> selectMapByName(Set<String> names)
	{
		fflib_QueryFactory query = initNewQueryFactory();
		query.setCondition('Name IN :names');
		List<Contract__c> records = (List<Contract__c>) Database.query(query.toSOQL());

		Map<String, Contract__c> resultMap = new Map<String, Contract__c>();
		for (Contract__c record: records) {
			resultMap.put(record.Name, record);
		}
		return resultMap;

	}


	public Contract__c getContract(Id contractId) {
		Contract__c returnContract = null;
		
		fflib_QueryFactory query = initNewQueryFactory();
		query.setCondition('Id = :contractId');
		query.selectField('Last_Owner_Trans__c');
		query.selectField('Membership_Number__c');
		query.selectField('Membership_Number__r.Developer__c');
		query.selectField('First_Current_Owner__c');
		query.selectField('Batch_Number__c');
		query.selectField('Contract_Number__c');
		query.selectField('Date_Signed__c');
		query.selectField('Expiration_Date__c');
		query.selectField('Lender__c');
		query.selectField('Lender_Id__c');
		query.selectField('Member_Type__c');
		query.selectField('Membership_Type__c');
		query.selectField('Points__c');
		query.selectField('Resort__c');
		query.selectField('Season__c');
		query.selectField('Last_Owner_Trans__c');
		List<Contract__c> contracts = (List<Contract__c>) Database.query(query.toSOQL());
		if (contracts.size() > 0) returnContract = contracts[0];
		return returnContract;
	}


}