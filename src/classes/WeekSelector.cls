public with sharing class WeekSelector extends fflib_SObjectSelector
{
	public List<Schema.SObjectField> getSObjectFieldList()
	{
		return new List<Schema.SObjectField> {
			Week__c.Id,
			Week__c.Name,
			Week__c.Resort__c,
			Week__c.Code__c,
			Week__c.ICN_Code__c
		};
	}
	
	public Schema.SObjectType getSObjectType()
	{
		return Week__c.sObjectType;
	}

	public List<Week__c> selectById(Set<ID> idSet)
	{
		return (List<Week__c>) selectSObjectsById(idSet);
	}

	public fflib_QueryFactory initNewQueryFactory() {
		fflib_QueryFactory query = newQueryFactory();
		return query;
	}

	public List<Week__c> selectByName(List<String> nameList)
	{
		fflib_QueryFactory query = initNewQueryFactory();
		query.setCondition('Name IN :nameList');
		return  (List<Week__c>) Database.query(query.toSOQL());
	}

	public List<Week__c> selectByNameAndResortId(List<String> weekNames, Id resortId)
	{
		fflib_QueryFactory query = initNewQueryFactory();
		query.setCondition('Name in :weekNames and Resort__c = :resortId');
		return  (List<Week__c>) Database.query(query.toSOQL());
	}

	public List<Week__c> selectByResortNameAndWeekName(String resortName, List<String> weekNames )
	{
		fflib_QueryFactory query = initNewQueryFactory();
		query.setCondition('Name in :weekNames and Resort__r.Name = :resortName');
		return  (List<Week__c>) Database.query(query.toSOQL());
	}

	public Map<String, Week__c> selectMapByCode(Set<String> codes)
	{
		fflib_QueryFactory query = initNewQueryFactory();
		query.setCondition('Code__c IN :codes');
		List<Week__c> records = (List<Week__c>) Database.query(query.toSOQL());

		Map<String, Week__c> resultMap = new Map<String, Week__c>();
		for (Week__c record: records) {
			resultMap.put(record.Code__c, record);
		}
		return resultMap;

	}


}