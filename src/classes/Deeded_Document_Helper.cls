/*
 * Developer: Bob Roos <bob.roos@getconga.com>
 * Description: 
 *    Provides functions related to Deeded Document actions
 */

public without sharing class Deeded_Document_Helper {

    public static void updateDocumentType(Map<Id, Deeded_Document__c> newMap  ) {
		if (newMap == null) return;
		List<Deeded_Document__c> documents = new Deeded_DocumentSelector().selectByIdAndHasDocType(newMap.keySet());

		for (Deeded_Document__c doc : documents) {
			newMap.get(doc.Id).Is_Deed_of_Trust__c = doc.Deeded_Document_Type__r.Is_Deed_of_Trust__c;
			newMap.get(doc.Id).Is_Developer_Grant_Deed__c = doc.Deeded_Document_Type__r.Is_Developer_Grant_Deed__c;
			newMap.get(doc.Id).Is_Reconveyance__c = doc.Deeded_Document_Type__r.Is_Reconveyance__c;
			newMap.get(doc.Id).Is_Trustees_Deed__c = doc.Deeded_Document_Type__r.Is_Trustees_Deed__c;
		}
	}
}