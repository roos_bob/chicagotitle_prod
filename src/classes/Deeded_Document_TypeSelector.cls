public with sharing class Deeded_Document_TypeSelector extends fflib_SObjectSelector
{
	public List<Schema.SObjectField> getSObjectFieldList()
	{
		return new List<Schema.SObjectField> {
			Deeded_Document_Type__c.Id,
			Deeded_Document_Type__c.Name
		};
	}
	
	public Schema.SObjectType getSObjectType()
	{
		return Deeded_Document_Type__c.sObjectType;
	}

	public List<Deeded_Document_Type__c> selectById(Set<ID> idSet)
	{
		return (List<Deeded_Document_Type__c>) selectSObjectsById(idSet);
	}
	
	public fflib_QueryFactory initNewQueryFactory() {
		fflib_QueryFactory query = newQueryFactory();
		return query;
	}

	public List<Deeded_Document_Type__c> selectByName(List<String> nameList)
	{
		fflib_QueryFactory query = initNewQueryFactory();
		query.setCondition('Name IN :nameList');
		return  (List<Deeded_Document_Type__c>) Database.query(query.toSOQL());
	}

	public List<Deeded_Document_Type__c> selectByName(Set<String> nameSet)
	{
		fflib_QueryFactory query = initNewQueryFactory();
		query.setCondition('Name IN :nameSet');
		return  (List<Deeded_Document_Type__c>) Database.query(query.toSOQL());
	}

	public Map<String, Deeded_Document_Type__c> selectMapByName(Set<String> names)
	{
		fflib_QueryFactory query = initNewQueryFactory();
		query.setCondition('Name IN :names');
		List<Deeded_Document_Type__c> records = (List<Deeded_Document_Type__c>) Database.query(query.toSOQL());

		Map<String, Deeded_Document_Type__c> resultMap = new Map<String, Deeded_Document_Type__c>();
		for (Deeded_Document_Type__c record: records) {
			resultMap.put(record.Name, record);
		}
		return resultMap;

	}


}