@isTest
public class wrapPointsContract_Test {
	static testMethod void validateSelected() {
		Resort__c resort = new Resort__c();
		resort.Name = 'Test Resort';
		insert resort;
		Contract__c c = new Contract__c();
		c.Name = 'Test contract';
		c.Resort__c = resort.Id;
		insert c;
		wrapPointsContract wpc = new wrapPointsContract(c);
		System.assert(wpc.selected == false);
	}
}