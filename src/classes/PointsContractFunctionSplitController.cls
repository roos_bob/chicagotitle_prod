public class PointsContractFunctionSplitController {
	private ApexPages.StandardController stdController;
	public Transaction__c splitTrans {get; set;}
	private Id originalContractId;
	public Contract__c originalContract {get; set;}
	public Contract__c newContract {get; set;}
	public Membership_Number__c membershipNumber {get; set;}
	public List<Transaction__c> originalTransactions {get; set;}
	public List<Owner__c> newOwnerList {get; set;}
	public Boolean ShowNewOwnerPage {get; set;}
	public Owner__c editOwner {get; set;}
	public boolean isCreateOwner {get; set;}
	public boolean isEditOwner {get; set;}
	public integer ownerRowNumber {get; set;}



	/*
	Constructor
	*/
	public PointsContractFunctionSplitController(ApexPages.StandardController stdController) {
		this.stdController = stdController;   // Tansaction__c controller
		this.splitTrans = (Transaction__c)stdController.getRecord();
		originalContractId = ApexPages.currentPage().getParameters().get('contractId');
		originalContract = new ContractSelector().getContract(originalContractId);
		newContract = new Contract__c();
		newOwnerList = new List<Owner__c>();
		splitTrans.ContractId__c = originalContractId;
		membershipNumber = new Membership_Number__c();
		ShowNewOwnerPage = false;
		isCreateOwner = false;
		isEditOwner = false;
	}
	
	public void	addOwnerRow() {
		isCreateOwner = true;
		Owner__c o = new Owner__c();
		o.Developer__c = originalContract.Membership_Number__r.Developer__c;
		newOwnerList.add(o);
		editOwner = o;
		ShowNewOwnerPage = true;
		

	}

	public void editOwnerRow() {
		isEditOwner = true;
		ShowNewOwnerPage = true;
		editOwner = newOwnerlist.get(ownerRowNumber-1);
	}
	public void deleteOwnerRow() {
		ShowNewOwnerPage = false;
		newOwnerList.remove(ownerRowNumber-1);
	}
	public void	removeOwnerRow() {
		Integer sz = newOwnerList.size() - 1;
		newOwnerList.remove(sz);
		
	}
	public void SaveNewOwnerPage() {
		ShowNewOwnerPage = false;
		isCreateOwner = false;
		isEditOwner = false;
	}
	public void HideNewOwnerPage() {
		ShowNewOwnerPage = false;
		if (isCreateOwner == true) {
			removeOwnerRow();
		}
		isCreateOwner = false;
		isEditOwner = false;

	}
	public PageReference saveAndReturn()
	{
		//stdController.save();
		Contract_Helper.newSplitTransaction(originalContractId, membershipNumber.Name, newContract.Contract_Number__c, splitTrans.Closing_Date__c , splitTrans.Comments__c, originalContract.Points__c, newContract.Points__c, newOwnerList);
		PageReference parentPage = new PageReference('/' + originalContractId);
		parentPage.setRedirect(true);
		return parentPage;
	}

}