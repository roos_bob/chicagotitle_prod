@isTest
private class Deeded_ContractSelectorTest {

	@isTest
	private static void testSelector() {
 		CS_Trigger_Setting__c newCS;
        newCS = new CS_Trigger_Setting__c(Name='Deeded_Contract__c.UpdateUIC', Enabled__c=false);
        insert newCS;
		newCS = new CS_Trigger_Setting__c(Name='Deeded_Contract__c.createTitlePolicies', Enabled__c=false);
        insert newCS;
		Deeded_Contract__c record = new Deeded_Contract__c(Name = 'test');
		insert record;

		Deeded_ContractSelector selector = new Deeded_ContractSelector();
		Set<Id> idSet = new Set<Id> {record.Id};
		List<String> nameList = new List<String> {'test'};
		Set<String> nameSet = new Set<String> {'test'};
		
		List<Deeded_Contract__c> byID = selector.selectById(idSet);
		System.assert(byId.size() == 1);

		List<Deeded_Contract__c> byName = selector.selectByName(nameList);
		System.assert(byName.size() == 1);
		
		Map<String, Deeded_Contract__c> mapByName = selector.selectMapByName(nameSet);
		System.assert(mapByName.size() == 1);

	}
}