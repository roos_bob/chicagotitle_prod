@isTest 
private class Title_PolicySelectorTest {

	@isTest
	private static void testSelector() {
		Title_Policy__c record = new Title_Policy__c();
		record.Name = 'Test';
		insert record;
		Title_Policy__c ts = [select id, name from Title_Policy__c where id = :record.Id];

		Set<Id> idSet = new Set<Id> {ts.Id};
		List<String> nameList = new List<String> {ts.Name};
		//Set<String> nameSet = new Set<String> {ts.Name};
		
		Title_PolicySelector selector = new Title_PolicySelector();

		List<Title_Policy__c> byID = selector.selectById(idSet);
		System.assert(byId.size() == 1);

		List<Title_Policy__c> byName = selector.selectByName(nameList);
		System.assert(byName.size() == 1);
		


	}
}